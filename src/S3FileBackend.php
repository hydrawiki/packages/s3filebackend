<?php
/**
 * Base class for all backends using particular storage medium.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup	FileBackend
 */

/*
$wgFileBackends[] = [
	'class'				=> 'S3FileBackend',
	'name'				=> 's3',
	'wikiId'			=> "mywikidb", //Replace this with the appropriate DS call to getDatabase()['dbname'].
	'lockManager'		=> 'nullLockManager',
	'awsRegion'			=> 'us-east-5',
	'bucket'			=> 'example-bucket',
	'cloudfrontId'		=> 'xxxxxxxxxxxxxx', (Optional) If using a Cloudfront presented S3 bucket configure the ID and domain.
	'cloudfrontDomain'	=> 'xxxx.cloudfront.net' (Optional) This will override presenting HTTP URLs as amazonaws.com URLs.
];
$wgLocalFileRepo = [
	'class' => 'LocalRepo',
	'name' => 'local',
	'backend' => 's3', //Has to match the 'name' key in the file backend configuration.
	'directory' => $wgUploadDirectory,
	'scriptDirUrl' => $wgScriptPath,
	'scriptExtension' => '.php',
	'url' => 'https://example-bucket.s3.us-east-5.amazonaws.com/mywikidb', //Replace this with the appropriate DS call to getDatabase()['dbname'].
	'hashLevels' => $wgHashedUploadDirectory ? 2 : 0,
	'thumbScriptUrl' => $wgThumbnailScriptPath,
	'transformVia404' => !$wgGenerateThumbnailOnParse,
	'deletedDir' => $wgDeletedDirectory,
	'deletedHashLevels' => $wgHashedUploadDirectory ? 3 : 0
];
*/

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use MediaWiki\MediaWikiServices;
use Wikimedia\Timestamp\ConvertibleTimestamp;

class S3FileBackend extends FileBackendStore {
	/**
	 * @var integer Seconds to store stats in the local cache
	 */
	const LOCAL_STAT_CACHE_TTL = 5;

	/**
	 * @var integer Seconds to store stats in the cluster cache
	 */
	const GLOBAL_STAT_CACHE_TTL = 2592000;

	/**
	 * AWS Region
	 *
	 * @var		string
	 */
	protected $region = '';

	/**
	 * AWS Bucket
	 *
	 * @var		string
	 */
	protected $bucket = null;

	/**
	 * S3 Client
	 *
	 * @var		object
	 */
	protected $s3 = null;

	/**
	 * Cloudfront Client
	 *
	 * @var		object
	 */
	protected $cf = null;

	/**
	 * Cloudfront ID
	 *
	 * @var		string
	 */
	protected $cfId = null;

	/**
	 * Cloudfront Domain
	 *
	 * @var		string
	 */
	protected $cfDomain = null;

	/**
	 * URL Template
	 *
	 * @var		string
	 */
	protected $urlTemplate = 'https://s3.%s.amazonaws.com/%s/%s';

	/**
	 * @see	FileBackend::__construct()
	 * Additional $config params include:
	 *   - awsRegion    : AWS Region
	 *
	 * @param	array	$config
	 */
	public function __construct(array $config) {
		global $wgFileStatCacheType;

		parent::__construct($config);

		$config['awsRegion'] = strtolower($config['awsRegion']);
		$this->region = $config['awsRegion'];
		$bucketName = $config['bucket'];

		$this->fileStatCache = ObjectCache::getInstance($wgFileStatCacheType);
		$this->fileStatLocalCache = MediaWikiServices::getInstance()->getLocalServerObjectCache();

		$this->s3 = new \Aws\S3\S3Client(
			[
				'version' => 'latest',
				'region'  => $config['awsRegion']
			]
		);

		if (!$this->s3) {
			//Will not function this.
			return;
		}

		if (isset($config['debug']) && $config['debug']) {
			try {
				$allBuckets = $this->s3->listBuckets();

				$bucket = $allBuckets->search("Buckets[?Name == '{$bucketName}']");
				if (is_array($bucket) && count($bucket) == 1) {
					$bucket = array_shift($bucket);
					if ($bucket['Name'] === $bucketName) {
						$this->bucket = $bucket;
					}
				}
			} catch (Exception $e) {
				wfDebug(__METHOD__.': '.$e->getMessage());
			}
		} else {
			$this->bucket = ['Name' => $bucketName];
		}

		if (isset($config['cloudfrontId']) && !empty($config['cloudfrontId']) && isset($config['cloudfrontDomain']) && !empty($config['cloudfrontDomain']) ) {
			$this->cfId = $config['cloudfrontId'];
			$this->cfDomain = $config['cloudfrontDomain'];
			$this->urlTemplate = 'https://'.$this->cfDomain.'/%s/%s';

			$this->cf = new \Aws\CloudFront\CloudFrontClient(
				[
					'version' => '2017-03-25',
					'region'  => $config['awsRegion']
				]
			);
		}
	}

	/**
	 * Return if our bucket is valid.
	 *
	 * @access	private
	 * @return	boolean
	 */
	private function haveValidBucket() {
		if (!empty($this->bucket) && strlen($this->bucket['Name'])) {
			return true;
		}
		return false;
	}

	/**
	 * Clear the stat cache for a file
	 *
	 * @access	private
	 * @param	string	$s3Key
	 */
	private function clearFileStatCache($s3Key) {
		$cacheKey = 'stats:' . base64_encode($s3Key);
		$this->fileStatCache->delete($cacheKey);
		$this->fileStatLocalCache->delete($cacheKey);
	}

	/**
	 * Get the cached stat for a file
	 *
	 * @access	private
	 * @param	string	$s3Key
	 * @return	array|false
	 */
	private function getCachedFileStat($s3Key) {
		$cacheKey = 'stats:' . base64_encode($s3Key);
		$localStat = $this->fileStatLocalCache->get($cacheKey);
		if ($localStat !== false) {
			return $localStat;
		}
		$stat = $this->fileStatCache->get($cacheKey);
		if ($stat !== false) {
			// Refresh the local cache on hit
			$this->fileStatLocalCache->set($cacheKey, $stat, self::LOCAL_STAT_CACHE_TTL);
			return $stat;
		}
		return false;
	}

	/**
	 * Set the cached stat for a file
	 *
	 * @access	private
	 * @param	string	$s3Key
	 * @param	array	$stat
	 */
	private function setCachedFileStat($s3Key, $stat) {
		$cacheKey = 'stats:' . base64_encode($s3Key);
		$this->fileStatLocalCache->set($cacheKey, $stat, self::LOCAL_STAT_CACHE_TTL);
		$this->fileStatCache->set($cacheKey, $stat, self::GLOBAL_STAT_CACHE_TTL);
	}

	/**
	 * Get the a bitfield of extra features supported by the backend medium
	 *
	 * @access	public
	 * @return	integer	Bitfield	of FileBackend::ATTR_* flags
	 */
	public function getFeatures() {
		return (FileBackend::ATTR_UNICODE_PATHS | FileBackend::ATTR_HEADERS | FileBackend::ATTR_METADATA);
	}

	/**
	 * Check if a file can be created or changed at a given storage path.
	 * FS backends should check if the parent directory exists, files can be
	 * written under it, and that any file already there is writable.
	 * Backends using key/value stores should check if the container exists.
	 *
	 * @access	public
	 * @param	string	$storagePath
	 * @return	boolean
	 */
	public function isPathUsableInternal($storagePath) {
		return $this->haveValidBucket();
	}

	/**
	 * @see	FileBackendStore::createInternal()
	 * @param	array	$params
	 * @return	StatusValue
	 */
	protected function doCreateInternal(array $params) {
		$status = $this->newStatus();

		$s3Key = $this->makeS3Key($params['dst']);
		if ($s3Key === null) {
			//Not something that S3 can handle or cares about.
			return $status;
		}

		$sha1Hash = sha1($params['content']);
		$contentType = isset($params['headers']['content-type']) ? $params['headers']['content-type'] : $this->getContentType($params['dst'], $params['content'], null);

		$this->clearFileStatCache($s3Key);

		$tagging = [
			'mtime'	=> time(),
			'sha1'	=> $sha1Hash,
			'size'	=> strlen($params['content']),
			'type'	=> $contentType
		];

		$requests = [
			[
				'ACL'			=> 'public-read',
				'Bucket'		=> $this->bucket['Name'],
				'Key'			=> $s3Key,
				'Body'			=> $params['content'],
				'ContentLength'	=> strlen($params['content']),
				'ContentType'	=> $contentType,
				'Tagging'		=> http_build_query($tagging)
			]
		];

		$method = __METHOD__;
		$asyncHandler = function(array $request, StatusValue $status) use ($method, $params) {
			try {
				$result = $this->s3->putObject($request);
			} catch (Exception $e) {
				$status->fatal('backend-fail-create', $params['src'], $params['dst']);
			}
		};

		$opHandle = new S3FileOpHandle($this, $asyncHandler, $requests);

		if (!empty($params['async'])) {
			$status->value = $opHandle;
		} else {
			$status->merge(current($this->executeOpHandlesInternal([$opHandle])));
		}

		return $status;
	}

	/**
	 * @see	FileBackendStore::storeInternal()
	 * @param	array	$params
	 * @return	StatusValue
	 */
	protected function doStoreInternal(array $params) {
		$status = $this->newStatus();

		$s3Key = $this->makeS3Key($params['dst']);
		if ($s3Key === null) {
			//Not something that S3 can handle or cares about.
			return $status;
		}

		MediaWiki\suppressWarnings();
		$sha1Hash = sha1_file($params['src']);
		MediaWiki\restoreWarnings();
		if ($sha1Hash === false) {
			$status->fatal('backend-fail-store', $params['src'], $params['dst']);
			return $status;
		}
		$sha1Hash = Wikimedia\base_convert($sha1Hash, 16, 36, 31);
		$contentType = isset($params['headers']['content-type']) ? $params['headers']['content-type'] : $this->getContentType($params['dst'], null, $params['src']);

		$fileHandle = fopen($params['src'], 'rb');
		if ($fileHandle === false) {
			$status->fatal('backend-fail-store', $params['src'], $params['dst']);
			return $status;
		}

		$this->clearFileStatCache($s3Key);

		$tagging = [
			'mtime'	=> intval(filemtime($params['src'])),
			'sha1'	=> $sha1Hash,
			'size'	=> filesize($params['src']),
			'type'	=> $contentType
		];

		$requests = [
			[
				'ACL'			=> 'public-read',
				'Bucket'		=> $this->bucket['Name'],
				'Key'			=> $s3Key,
				'Body'			=> $fileHandle,
				'ContentLength'	=> filesize($params['src']),
				'ContentType'	=> $contentType,
				'Tagging'		=> http_build_query($tagging)
			]
		];

		$method = __METHOD__;
		$asyncHandler = function(array $request, StatusValue $status) use ($method, $params) {
			try {
				$result = $this->s3->putObject($request);
			} catch (Exception $e) {
				$status->fatal('backend-fail-store', $params['src'], $params['dst']);
			}
		};

		$opHandle = new S3FileOpHandle($this, $asyncHandler, $requests);
		$opHandle->resourcesToClose[] = $fileHandle;

		if (!empty($params['async'])) {
			$status->value = $opHandle;
		} else {
			$status->merge(current($this->executeOpHandlesInternal([$opHandle])));
		}

		return $status;
	}

	/**
	 * @see	FileBackendStore::copyInternal()
	 * @param	array	$params
	 * @return	StatusValue
	 */
	protected function doCopyInternal(array $params) {
		$status = $this->newStatus();

		$s3KeySrc = $this->makeS3Key($params['src']);
		if ($s3KeySrc === null) {
			//Not something that S3 can handle or cares about.
			return $status;
		}

		$s3KeyDst = $this->makeS3Key($params['dst']);
		if ($s3KeyDst === null) {
			//Not something that S3 can handle or cares about.
			return $status;
		}

		$this->clearFileStatCache($s3KeySrc);
		$this->clearFileStatCache($s3KeyDst);

		$requests = [
			[
				'ACL'			=> 'public-read',
				'Bucket'		=> $this->bucket['Name'],
				'Key'			=> $s3KeyDst,
				'CopySource'	=> "/{$this->bucket['Name']}/".urlencode($s3KeySrc)
			]
		];

		$method = __METHOD__;
		$asyncHandler = function(array $request, StatusValue $status) use ($method, $params) {
			try {
				$result = $this->s3->copyObject($request);
			} catch (Exception $e) {
				$status->fatal('backend-fail-move', $params['src'], $params['dst']);
			}
		};

		$opHandle = new S3FileOpHandle($this, $asyncHandler, $requests);

		if (!empty($params['async'])) {
			$status->value = $opHandle;
		} else {
			$status->merge(current($this->executeOpHandlesInternal([$opHandle])));
		}

		return $status;
	}

	/**
	 * @see	FileBackendStore::deleteInternal()
	 * @param	array	$params
	 * @return	StatusValue
	 */
	protected function doDeleteInternal(array $params) {
		$status = $this->newStatus();

		$s3KeySrc = $this->makeS3Key($params['src']);
		if ($s3KeySrc === null) {
			//Not something that S3 can handle or cares about.
			return $status;
		}

		$this->clearFileStatCache($s3KeySrc);

		$requests = [
			[
				'Bucket'		=> $this->bucket['Name'],
				'Key'			=> $s3KeySrc
			]
		];

		$method = __METHOD__;
		$asyncHandler = function(array $request, StatusValue $status) use ($method, $params) {
			try {
				$result = $this->s3->deleteObject($request);
			} catch (Exception $e) {
				$status->fatal('backend-fail-delete', $params['src']);
			}
		};

		$opHandle = new S3FileOpHandle($this, $asyncHandler, $requests);

		if (!empty($params['async'])) {
			$status->value = $opHandle;
		} else {
			$status->merge(current($this->executeOpHandlesInternal([$opHandle])));
		}

		return $status;
	}

	/**
	 * @see	FileBackendStore::doSecure()
	 * @param	string	$container
	 * @param	string	$dir
	 * @param	array	$params
	 * @return	StatusValue
	 */
	protected function doSecureInternal($container, $dir, array $params) {
		$s3Key = $this->makeS3Key($params['dir']);
		try {
			$result = $this->s3->putObjectAcl(
				[
					'ACL'		=> 'private|authenticated-read|aws-exec-read|bucket-owner-read|bucket-owner-full-control',
					'Bucket'	=> $this->bucket['Name'],
					'Key'		=> $s3Key
				]
			);
		} catch (Exception $e) {
			wfDebug(__METHOD__.": Failed to put ACL on object.");
		}

		$this->clearFileStatCache($s3Key);

		return $this->newStatus();
	}

	/**
	 * @see	FileBackendStore::getFileStat()
	 * @param	array	$params
	 */
	protected function doGetFileStat(array $params) {
		if (!$this->haveValidBucket()) {
			return null;
		}

		$s3Key = $this->makeS3Key($params['src']);
		if ($s3Key === null) {
			//Not something that S3 can handle or cares about.
			return false;
		}

		$cache = $this->getCachedFileStat($s3Key);
		if ($cache !== false && is_array($cache)) {
			if (isset($cache['missing']) && $cache['missing'] == true) {
				return false;
			}
			return $cache;
		}

		$tags = [];
		$stats = [
			'missing' => true
		];

		try {
			$tags = $this->s3->getObjectTagging([
				'Bucket'	=> $this->bucket['Name'],
				'Key'		=> $s3Key
			]);
		} catch (S3Exception $e) {
			if ($e->getAwsErrorCode() === 'NoSuchKey') {
				$this->setCachedFileStat($s3Key, $stats);
				return false;
			}
			return null;
		} catch (AwsException $e) {
			if ($e->getAwsErrorCode() === 'NoSuchKey') {
				$this->setCachedFileStat($s3Key, $stats);
				return false;
			}
			return null;
		} catch (Exception $e) {
			return null;
		}

		if (isset($tags['TagSet'])) {
			foreach ($tags['TagSet'] as $set) {
				if ($set['Key'] == 'mtime') {
					$timestamp = new ConvertibleTimestamp($set['Value']);
					$stats[$set['Key']] = $timestamp->getTimestamp(TS_MW);
					continue;
				}
				$stats[$set['Key']] = $set['Value'];
			}
		}
		unset($stats['missing']);
		$this->setCachedFileStat($s3Key, $stats);
		return $stats;
	}

	/**
	 * @see	FileBackendStore::getLocalCopyMulti()
	 * @param	array	$params
	 * @return	array
	 */
	protected function doGetLocalCopyMulti(array $params) {
		$validBucket = $this->haveValidBucket();
		if (!is_array($params['srcs'])) {
			return [];
		}

		$tmpFiles = [];

		foreach ($params['srcs'] as $source) {
			if (!$validBucket) {
				$tmpFiles[$source] = null;
				continue;
			}

			try {
				$file = $this->s3->getObject([
					'Bucket'	=> $this->bucket['Name'],
					'Key'		=> $this->makeS3Key($source)
				]);
			} catch (Exception $e) {
				$tmpFiles[$source] = null;
				continue;
			}
			$extension = FileBackend::extensionFromPath($source);

			$tmpFile = TempFSFile::factory('localcopy_', $extension, $this->tmpDirectory);
			if ($tmpFile) {
				$handle = fopen($tmpFile->getPath(), 'wb');
				if (fwrite($handle, $file['Body']->getContents()) !== false) {
					$tmpFiles[$source] = $tmpFile;
					fclose($handle);
				} else {
					$tmpFiles[$source] = null;
				}
			}
		}

		return $tmpFiles;
	}

	/**
	 * @see	FileBackend::getFileHttpUrl()
	 * @param	array	$params
	 * @return	string|null
	 */
	public function getFileHttpUrl(array $params) {
		//@TODO: This is untested.  Nothing seems to actually ever call this function up the stack.
		return sprintf($this->urlTemplate, $this->region, $this->bucket['Name'], $this->makeS3Key($params['src']));
	}

	/**
	 * @see	FileBackendStore::directoryExists()
	 *
	 * @param	string	$container Resolved container name
	 * @param	string	$dir Resolved path relative to container
	 * @param	array	$params
	 * @return	mxied	boolean|null
	 */
	protected function doDirectoryExists($container, $dir, array $params) {
		if (!$this->haveValidBucket()) {
			return null;
		}

		$prefixDir = trim($params['dir'], '/');

		try {
			$prefix = $this->makeS3Key($prefixDir);
			$result = $this->s3->listObjectsV2(
				[
				    'Bucket' => $this->bucket['Name'],
				    'MaxKeys' => 1,
				    'Prefix' => $prefix
				]
			);

			if (isset($result['KeyCount']) && $result['KeyCount'] > 0) {
				return true;
			}
			return false;
		} catch (Exception $e) {
			throw new FileBackendError('S3 Error: '.$e->getMessage());
		}
		return null;
	}

	/**
	 * Do not call this function from places outside FileBackend
	 *
	 * @see	FileBackendStore::getDirectoryList()
	 *
	 * @param	string	$container Resolved container name
	 * @param	string	$dir Resolved path relative to container
	 * @param	array	$params
	 * @return	Traversable|array|null	Returns null on failure
	 */
	public function getDirectoryListInternal($container, $dir, array $params) {
		return new S3FileBackendDirList($this, $container, $dir, $params);
	}

	/**
	 * Do not call this function from places outside FileBackend
	 *
	 * @see	FileBackendStore::getFileList()
	 *
	 * @param	string	$container Resolved container name
	 * @param	string	$dir Resolved path relative to container
	 * @param	array	$params
	 * @return	Traversable|array|null	Returns null on failure
	 */
	public function getFileListInternal($container, $dir, array $params) {
		return new S3FileBackendFileList($this, $container, $dir, $params);
	}

	/**
	 * Do not call this function outside of S3FileBackendFileList
	 *
	 * @param	string	$fullCont Resolved container name
	 * @param	string	$dir Resolved storage directory with no trailing slash
	 * @param	string|null	$after Resolved container relative path to list items after
	 * @param	integer	$limit Max number of items to list
	 * @param	array	$params Parameters for getDirectoryList()
	 * @return	array	List of container relative resolved paths of directories directly under $dir
	 * @throws	FileBackendError
	 */
	public function getDirListPageInternal($fullCont, $dir, &$after, $limit, array $params) {
		$files = [];
		if ($after === INF) {
			return $files; //This is the end, it's ending!
		}

		$prefixDir = trim($params['dir'], '/');
		$dirDepth = substr_count($prefixDir, '/') + 1; //Add trailing to make count match below.

		list(, $shortCont,) = FileBackend::splitStoragePath($prefixDir);
		list($fileRepoName) = explode('-', $shortCont);
		try {
			$prefix = $this->makeS3Key($prefixDir);
			if ($after !== null && $after !== INF) {
				try {
					$result = $this->s3->listObjectsV2(
						[
						    'Bucket' => $this->bucket['Name'],
						    'ContinuationToken' => $after,
						    'MaxKeys' => intval($limit)
						]
					);
				} catch (Exception $e) {
					throw new FileBackendError("Iterator page I/O error.");
				}
			} else {
				try {
					$result = $this->s3->listObjectsV2(
						[
						    'Bucket' => $this->bucket['Name'],
						    'MaxKeys' => intval($limit),
						    'Prefix' => $prefix
						]
					);
				} catch (Exception $e) {
					throw new FileBackendError("Iterator page I/O error.");
				}
			}
			if (!$result['IsTruncated']) {
				$after = INF;
			} else {
				$after = $result['NextContinuationToken'];
			}

			if (!isset($result['Contents']) || (isset($result['KeyCount']) && $result['KeyCount'] == 0)) {
				return $files;
			}

			foreach ($result['Contents'] as $content) {
				$storePath = str_replace($prefixDir.'/', '', $this->makeStorePath($content['Key'], $fileRepoName));
				$parts = explode('/', $storePath);
				array_pop($parts); //Nuke the last one since it is a file name.  Virtual directories only exist in S3 if there is a file there to make it exist.
				if (isset($params['topOnly']) && $params['topOnly'] === true) {
					$parts = [array_shift($parts)];
				}
				$testDir = $prefixDir;
				foreach ($parts as $part) {
					$testDir .= '/'.$part;
					if (!isset($files[$testDir])) {
						$files[$testDir] = str_replace($prefixDir.'/', '', $testDir);
					}
				}
			}
			$files = array_values($files);
		} catch (Exception $e) {
			throw new FileBackendError( "Iterator page I/O error." );
		}
		return $files;
	}

	/**
	 * Do not call this function outside of S3FileBackendFileList
	 *
	 * @access	public
	 * @param	string	$fullCont Resolved container name
	 * @param	string	$dir Resolved storage directory with no trailing slash
	 * @param	string|null	$after Continuation Token
	 * @param	integer	$limit Max number of items to list
	 * @param	array	$params Parameters for getDirectoryList()
	 * @return	array	List of container relative resolved paths of directories directly under $dir
	 * @throws	FileBackendError
	 */
	public function getFileListPageInternal($fullCont, $dir, &$after, $limit, array $params) {
		$files = [];
		if ($after === INF) {
			return $files; //This is the end, it's ending!
		}

		$prefixDir = trim($params['dir'], '/');
		$dirDepth = substr_count($prefixDir, '/') + 1; //Add trailing to make count match below.

		list(, $shortCont,) = FileBackend::splitStoragePath($prefixDir);
		list($fileRepoName) = explode('-', $shortCont);
		try {
			$prefix = $this->makeS3Key($prefixDir);
			if ($after !== null && $after !== INF) {
				try {
					$result = $this->s3->listObjectsV2(
						[
						    'Bucket' => $this->bucket['Name'],
						    'ContinuationToken' => $after,
						    'MaxKeys' => intval($limit)
						]
					);
				} catch (Exception $e) {
					throw new FileBackendError("Iterator page I/O error.");
				}
			} else {
				try {
					$result = $this->s3->listObjectsV2(
						[
						    'Bucket' => $this->bucket['Name'],
						    'MaxKeys' => intval($limit),
						    'Prefix' => $prefix
						]
					);
				} catch (Exception $e) {
					throw new FileBackendError("Iterator page I/O error.");
				}
			}
			if (!$result['IsTruncated']) {
				$after = INF;
			} else {
				$after = $result['NextContinuationToken'];
			}

			if (!isset($result['Contents']) || (isset($result['KeyCount']) && $result['KeyCount'] == 0)) {
				return $files;
			}

			foreach ($result['Contents'] as $content) {
				if (isset($params['topOnly']) && $params['topOnly'] === true && substr_count($this->makeStorePath($content['Key'], $fileRepoName), '/') != $dirDepth) {
					continue; //This file is above or below this directory.
				}
				$files[] = str_replace($prefixDir.'/', '', $this->makeStorePath($content['Key'], $fileRepoName));
			}
		} catch (Exception $e) {
			throw new FileBackendError( "Iterator page I/O error." );
		}
		return $files;
	}

	/**
	 * @see	FileBackendStore::executeOpHandlesInternal()
	 *
	 * @param	FileBackendStoreOpHandle[]	$fileOpHandles
	 *
	 * @throws	FileBackendError
	 * @return	StatusValue[]	List of corresponding StatusValue objects
	 */
	protected function doExecuteOpHandlesInternal(array $fileOpHandles) {
		$statuses = [];
		if (is_array($fileOpHandles)) {
			foreach ($fileOpHandles as $index => $fileOpHandle) {
				$statuses[$index] = $this->newStatus();
				$callback = $fileOpHandles[$index]->callback;
				foreach ($fileOpHandle->putObjects as $putObject) {
					call_user_func_array($callback, [$putObject, $statuses[$index]]);
					if (!$statuses[$index]->isOK()) {
						break;
					}
					if (isset($putObject['Key'])) {
						$this->invalidateCloudfront($putObject['Key']);
					}
				}
			}
		}

		return $statuses;
	}

	/**
	 * Get file stat information (concurrently if possible) for several files
	 *
	 * @see	FileBackend::getFileStat()
	 *
	 * @param	array	$params Parameters include:
	 *   - srcs        : list of source storage paths
	 *   - latest      : use the latest available data
	 * @return	array|null	Map of storage paths to array|bool|null (returns null if not supported)
	 * @since	1.23
	 */
	protected function doGetFileStatMulti(array $params) {
		$stats = [];
		foreach ($params['srcs'] as $source) {
			$stats[$source] = $this->doGetFileStat(['src' => $source]);
		}
		return $stats;
	}

	/**
	 * Is this a key/value store where directories are just virtual?
	 *
	 * @access	protected
	 * @return	boolean
	 */
	protected function directoriesAreVirtual() {
		return true; //Duh.
	}

	/**
	 * Make S3 Compatible Key.
	 *
	 * @access	private
	 * @param	string	Path to process.
	 * @return	mixed	String key or null on a bad path.
	 */
	private function makeS3Key($path) {
		list($base, $tail) = $this->resolveStoragePathReal($path);
		if (empty($base)) {
			return null;
		}

		$baseParts = explode('-', $base);

		if (isset($baseParts[2]) && $baseParts[2] == 'public') {
			unset($baseParts[2]);
		}

		//Backwards compatibility fix for Extension:Math and Extension:Timeline.  Only FSFileBackend supports containerPaths.
		if (isset($baseParts[2]) && $baseParts[2] == 'render') {
			$baseParts[2] = $baseParts[1];
		}

		unset($baseParts[1]);
		$baseParts[] = $tail;
		return implode('/', $baseParts);
	}

	/**
	 * Make mwstore:// compatible path.
	 *
	 * @access	private
	 * @param	string	Key to process.
	 * @param	string	File Repository Name, usually from $wgLocalFileRepo.
	 * @return	mixed	String key.
	 */
	private function makeStorePath($key, $fileRepoName) {
		$public = false;
		if (preg_match('#^wikiauth/[a-z0-9]{1}/#', $key) > 0) {
			//Add public back on.
			$public = true;
		}
		return 'mwstore://'.substr_replace($key, $this->name.'/'.$fileRepoName.'-'.($public ? 'public' : ''), 0, strlen($this->domainId.'/'));
	}

	/**
	 * Invalidate Cloudfront distributions.
	 *
	 * @access	private
	 * @param	string	Path to invalidate.
	 * @return	void
	 */
	private function invalidateCloudfront($path) {
		if (isset($this->cfId)) {
			try {
				$result = $this->cf->createInvalidation(
					[
						'DistributionId' => $this->cfId,
						'InvalidationBatch' => [
							'CallerReference' => $path.'|'.time(),
							'Paths' => [
								'Items' => ['/'.$path],
								'Quantity' => 1,
							],
						]
					]
				);
			} catch (Exception $e) {
				wfDebug(__METHOD__.': Error attempting to invalidate Cloudfront - '.$e->getMessage());
			}
		}
	}
}

/**
 * FileBackendStore helper class for performing asynchronous file operations.
 *
 * For example, calling FileBackendStore::createInternal() with the "async"
 * param flag may result in a StatusValue that contains this object as a value.
 * This class is largely backend-specific and is mostly just "magic" to be
 * passed to FileBackendStore::executeOpHandlesInternal().
 */
class S3FileOpHandle extends FileBackendStoreOpHandle {
	/**
	 * File backend that initialized this transaction.
	 *
	 * @access	public
	 * @var		object	S3FileBackend
	 */
	public $backend;

	/**
	 * Callback for finalizing the transaction.
	 *
	 * @access	public
	 * @var		object	Closure
	 */
	public $callback;

	/**
	 * Requests to putObjects.
	 *
	 * @access	public
	 * @var		array	Key	Value Arrays
	 */
	public $putObjects;

	/**
	 * @access	public
	 * @param	object	S3FileBackend	$backend
	 * @param	object	Closure	$callback Function that takes (HTTP request array, status)
	 * @param	array	S3	putObject key value arrays.
	 * @return	void
	 */
	public function __construct(S3FileBackend $backend, Closure $callback, array $putObjects) {
		$this->backend = $backend;
		$this->callback = $callback;
		$this->putObjects = $putObjects;
	}
}

/**
 * S3FileBackend helper class to page through listings.
 * S3 also has a listing limit of 10,000 objects for sanity.
 * Do not use this class from places outside S3FileBackend.
 *
 * @ingroup	FileBackend
 */
abstract class S3FileBackendList implements Iterator {
	/** @var	array List of path or (path,stat array) entries */
	protected $bufferIter = [];

	/** @var	string List items *after* this path */
	protected $bufferAfter = null;

	/** @var	int */
	protected $pos = 0;

	/** @var	array */
	protected $params = [];

	/** @var	S3FileBackend */
	protected $backend;

	/** @var	string Container name */
	protected $container;

	/** @var	string Storage directory */
	protected $dir;

	/** @var	int */
	protected $suffixStart;

	const PAGE_SIZE = 9000; // file listing buffer size

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @param	object	S3FileBackend	$backend
	 * @param	string	$fullCont	Resolved container name
	 * @param	string	$dir	Resolved directory relative to container
	 * @param	array	$params
	 */
	public function __construct(S3FileBackend $backend, $container, $dir, array $params) {
		$this->backend = $backend;
		$this->container = $container;
		$this->dir = $dir;
		if (substr($this->dir, -1) === '/') {
			$this->dir = substr($this->dir, 0, -1); // remove trailing slash
		}
		if ($this->dir == '') { // whole container
			$this->suffixStart = 0;
		} else { // dir within container
			$this->suffixStart = strlen($this->dir) + 1; // size of "path/to/dir/"
		}
		$this->params = $params;
	}

	/**
	 * @see	Iterator::key()
	 * @return	int
	 */
	public function key() {
		return $this->pos;
	}

	/**
	 * @see	Iterator::next()
	 */
	public function next() {
		// Advance to the next file in the page
		next($this->bufferIter);
		++$this->pos;
		// Check if there are no files left in this page and
		// advance to the next page if this page was not empty.
		if (!$this->valid() && count($this->bufferIter)) {
			$this->bufferIter = $this->pageFromList($this->container, $this->dir, $this->bufferAfter, self::PAGE_SIZE, $this->params
			); // updates $this->bufferAfter
		}
	}

	/**
	 * @see	Iterator::rewind()
	 */
	public function rewind() {
		$this->pos = 0;
		$this->bufferAfter = null;
		$this->bufferIter = $this->pageFromList(
			$this->container, $this->dir, $this->bufferAfter, self::PAGE_SIZE, $this->params
		); // updates $this->bufferAfter
	}

	/**
	 * @see	Iterator::valid()
	 * @return	bool
	 */
	public function valid() {
		if ($this->bufferIter === null) {
			return false; // some failure?
		} else {
			return (current($this->bufferIter) !== false); // no paths can have this value
		}
	}

	/**
	 * Get the given list portion (page)
	 *
	 * @param	string	$container Resolved container name
	 * @param	string	$dir Resolved path relative to container
	 * @param	string	$after
	 * @param	int	$limit
	 * @param	array	$params
	 * @return	Traversable|array
	 */
	abstract protected function pageFromList($container, $dir, &$after, $limit, array $params);
}

/**
 * Iterator for listing directories
 */
class S3FileBackendDirList extends S3FileBackendList {
	/**
	 * @see	Iterator::current()
	 * @return	string|bool	String (relative path) or false
	 */
	public function current() {
		return substr(current($this->bufferIter), $this->suffixStart, -1);
	}

	protected function pageFromList($container, $dir, &$after, $limit, array $params) {
		return $this->backend->getDirListPageInternal($container, $dir, $after, $limit, $params);
	}
}

/**
 * Iterator for listing regular files
 */
class S3FileBackendFileList extends S3FileBackendList {
	/**
	 * @see	Iterator::current()
	 * @return	string|bool	String (relative path) or false
	 */
	public function current() {
		return current($this->bufferIter);
	}

	protected function pageFromList($container, $dir, &$after, $limit, array $params) {
		return $this->backend->getFileListPageInternal($container, $dir, $after, $limit, $params);
	}
}
