# S3FileBackend

## Installation
Add this dependecy to your MediaWiki installation using Composer:

`composer require hydrawiki/s3filebackend`

## Example Configuration
```
$wgFileBackends[] = [
	'class'				=> 'S3FileBackend',
	'name'				=> 's3',
	'wikiId'			=> "mywikidb", //Replace this with the appropriate DS call to getDatabase()['dbname'].
	'lockManager'		=> 'nullLockManager',
	'awsRegion'			=> 'us-east-5',
	'bucket'			=> 'example-bucket',
	'cloudfrontId'		=> 'xxxxxxxxxxxxxx', (Optional) If using a Cloudfront presented S3 bucket configure the ID and domain.
	'cloudfrontDomain'	=> 'xxxx.cloudfront.net' (Optional) This will override presenting HTTP URLs as amazonaws.com URLs.
];
$wgLocalFileRepo = [
	'class' => 'LocalRepo',
	'name' => 'local',
	'backend' => 's3', //Has to match the 'name' key in the file backend configuration.
	'directory' => $wgUploadDirectory,
	'scriptDirUrl' => $wgScriptPath,
	'scriptExtension' => '.php',
	'url' => 'https://example-bucket.s3.us-east-5.amazonaws.com/mywikidb', //Replace this with the appropriate DS call to getDatabase()['dbname'].
	'hashLevels' => $wgHashedUploadDirectory ? 2 : 0,
	'thumbScriptUrl' => $wgThumbnailScriptPath,
	'transformVia404' => !$wgGenerateThumbnailOnParse,
	'deletedDir' => $wgDeletedDirectory,
	'deletedHashLevels' => $wgHashedUploadDirectory ? 3 : 0
];
```